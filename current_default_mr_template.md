## What does this MR do and why?

_Describe in detail what your merge request does and why._

<!--
Please keep this description updated with any discussion that takes place so
that reviewers can understand your intent. Keeping the description updated is
especially important if they didn't participate in the discussion.

-->

## Screenshots or screen recordings 

_These are strongly recommended to assist reviewers and reduce the time to merge your change._

<!-- 

Please include any relevant screenshots or screen recordings that will assist
reviewers and future readers. If you need help visually verifying the change,
please leave a comment and ping a GitLab reviewer, maintainer, or MR coach.

-->

## How to set up and validate locally 

_Numbered steps to set up and validate the change are strongly suggested._

<!-- 

Example below:
 
1. Enable the invite modal
   ```ruby
   Feature.enable(:invite_members_group_modal)
   ```
1. In rails console enable the experiment fully
   ```ruby
   Feature.enable(:member_areas_of_focus)
   ```
1. Visit any group or project member pages such as `http://127.0.0.1:3000/groups/flightjs/-/group_members`
1. Click the `invite members` button.

-->

## MR acceptance checklist

These checklists encourage us to confirm any changes have been analyzed to reduce risks in quality, performance, reliability, security, and maintainability. 

<!---
Using checklists improves quality in software engineering and other jobs such as with surgeons and airline pilots. This checklist is a straightforward tool to support and bolster the skills of contributors to the GitLab codebase.
More reading on checklists can be found in the "Checklist Manifesto": http://atulgawande.com/book/the-checklist-manifesto/

"It is common to misconceive how checklists function in complex lines of work. They are not comprehensive how-to guides, whether for building a skyscraper or getting a plane out of trouble. They are quick and simple tools aimed to buttress the skills of expert professionals." - Gawande, Atul. The Checklist Manifesto

If you have feedback on this template, please add a comment in https://gitlab.com/gitlab-org/gitlab/-/issues/340319
--->


### Quality

* [ ] Confirmed

<!---
See the test engineering process for further quality guidelines: https://about.gitlab.com/handbook/engineering/quality/test-engineering/
--->

1. I have self-reviewed this MR per [code review guidelines](https://docs.gitlab.com/ee/development/code_review.html).
1. For the code that that this change impacts, I believe that the automated tests ([Testing Guide](https://docs.gitlab.com/ee/development/testing_guide/index.html)) validate functionality that is highly important to users (including consideration of [all test levels](https://docs.gitlab.com/ee/development/testing_guide/testing_levels.html)).  If the existing automated tests do not cover this functionality, I have added the necessary additional tests or I have added an issue to describe the automation testing gap and linked it to this MR.  
1. I have considered the technical aspects of the impact of this change on both gitlab.com hosted customers and self-hosted customers.
1. I have considered the impact of this change on the front-end, back-end, and database portions of the system where appropriate and applied ~frontend, ~backend and ~database labels accordingly.
1. I have tested this MR in [all supported browsers](https://docs.gitlab.com/ee/install/requirements.html#supported-web-browsers), or determiend that this testing is not needed.
1. I have confirmed that this change is [backwards compatible across updates](https://docs.gitlab.com/ee/development/multi_version_compatibility.html), or I have decided that this does not apply.
1. I have properly separated EE content from FOSS, or this MR is FOSS only. ([Where should EE code go?](https://docs.gitlab.com/ee/development/ee_features.html#separation-of-ee-code))
1. If I am introducing a new expectation for existing data, I have confirmed that existing data meets this expectation or I have made this expectation optional rather than required.

### Performance, reliability and availability

* [ ] Confirmed 

1. I am confident that this MR does not harm performance, or I have asked a reviewer to help assess the performance impact. ([Merge request performance guidelines](https://docs.gitlab.com/ee/development/merge_request_performance_guidelines.html))
1. I have added [information for database reviewers in the MR description](https://docs.gitlab.com/ee/development/database_review.html#required), or I have decided that it is unnecessary. ([Does this MR have database-related changes?](https://docs.gitlab.com/ee/development/database_review.html))
1. I have considered the availability and reliability risks of this change.  I have also considered the scalability risk based on future predicted growth
1. I have considered the performance, reliability and availability impacts of this change on large customers who may have significantly more data than the average customer.

### Documentation

* [ ] Confirmed

1. I have included changelog trailers, or I have decided that they are not needed. ([Does this MR need a changelog?](https://docs.gitlab.com/ee/development/changelog.html#what-warrants-a-changelog-entry))
1. I have added/updated documentation, or I have decided that documentation changes are not needed for this MR. ([Is documentation required?](https://about.gitlab.com/handbook/engineering/ux/technical-writing/workflow/#when-documentation-is-required))

### Security

* [ ] Confirmed

1. I have confirmed that if this MR contains changes to processing or storing of credentials or tokens, authorization, and authentication methods, or other items described in [the security review guidelines](https://about.gitlab.com/handbook/engineering/security/#when-to-request-a-security-review), I have added the label ~security and I have `@`-mentioned `@gitlab-com/gl-security/appsec`.

### Deployment

* [ ] Confirmed

1. I have considered using a feature flag for this change because the change may be high risk.  If I decided to use a feature flag, I plan to test the change in staging before I test it in production, and I have considered rolling it out to a subset of production customers before doing rolling it out to all customers.  [When to use a feature flag](https://about.gitlab.com/handbook/product-development-flow/feature-flag-lifecycle/#when-to-use-feature-flags)
1. I have informed the Infrastructure department of a default setting or new setting change per [definition of done](https://docs.gitlab.com/ee/development/contributing/merge_request_workflow.html#definition-of-done), or decided that this is not needed.
